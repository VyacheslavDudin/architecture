# Architecture
I have chosen the DDD architecture, because of these various reasons: 
1. This approach makes it easier to test the domain layer so you can be sure that all your business requirements are met and you can write long-lived, error-free code.
2. DDD, in combination with layered architecture, avoids the domino effect, when a change in code in one place leads to innumerable bugs in different places.
3. We have a lot of different processes, services and models-entities,  and the DDD architecture is based on entities.
# Protocols
Since we don`t need to send a lot of messages between server and clients, in my mind, choosing a simple http protocol is a good idea. We shouldn`t use WebSocket or RabbitMQ, because their advantages of proccessing a large number of messages are unneccessary in this case.   